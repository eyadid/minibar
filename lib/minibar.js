var Minibar = module.exports = {
	views_cache : {},
	blocks : [],
	node : function(props){
		function NODE(_index){this.baseConstructor(_index);};
		NODE.prototype = Object.create(Minibar.Node.prototype);
		NODE.prototype.baseConstructor = Minibar.Node;
		NODE.prototype.constructor = props.constructor || NODE;
		NODE.prototype.type = props.type;
		return NODE;
	},
	block : function(props){
		var NODE = props._create ? props._create : function(_index){this.baseConstructor(_index);};
		NODE.prototype = Object.create(Minibar.Block.prototype);

		NODE.prototype._open = NODE._open = props._open;
		NODE.prototype._close = NODE._close = props._close;
		NODE.prototype._selfClosing = NODE._selfClosing = props._selfClosing;

		NODE.prototype.constructor = NODE;
		NODE.prototype.type = props.type;
		NODE.prototype._subNodes = props._subNodes;
		NODE.prototype._onClose = props._onClose || function(endIndx){
			return this._parent.push(new Minibar.Text(endIndx));
		};
		if(props._open && !props._subnode)
			Minibar.blocks.push(NODE);

		return NODE;
	},
	parse : function parse(content){
		var offset = 0, start = -1, end = -1, intStart = -1;
		var rootNode = new Minibar.Root();

		if(!content || !content.length)
			return rootNode;

		var currentTextNode = rootNode.push(new Minibar.Text(0));
		var DELIMITER_START = "{{";
		var DELIMITER_END = "}}";
		do {
			do {
				start = content.indexOf(DELIMITER_START,offset);
				if(start == -1) break;

				end = content.indexOf(DELIMITER_END, start + DELIMITER_START.length); 
				if(end == -1) break;

				intStart = content.indexOf(DELIMITER_START, start + DELIMITER_START.length);

				if(intStart == -1 || intStart > end){
					offset = end + DELIMITER_END.length;
					break;
				}

				offset = intStart;				

			}while(true);

			if(start == -1 || end == -1)
				break;			
				
			var match = null;
			var tag = (content.substring(start+2,end));
	
			// close parent ?
			if(currentTextNode._parent && currentTextNode._parent._close && tag.match(currentTextNode._parent._close)){
				currentTextNode.text = content.substring(currentTextNode._index, start);
				currentTextNode = currentTextNode._parent._onClose(end+2);
				continue;
			}
			
			function checkNext(blocks){
				for(var i = 0; i < blocks.length; i++){
					if(!(match = tag.match(blocks[i]._open))) continue;					
					currentTextNode.text = content.substring(currentTextNode._index, start);					
					var n = new blocks[i](match);
					currentTextNode._parent.push(n);
					if(blocks[i]._selfClosing)
						currentTextNode = currentTextNode._parent.push(new Minibar.Text(end+2));
					else
						currentTextNode = n.push(new Minibar.Text(end+2));					
						
					return true;
				}
				return false;
			}

			// Parent not closed. see if we have nested
			if(checkNext(Minibar.blocks))
				continue;

			if(currentTextNode._parent && currentTextNode._parent._subNodes && currentTextNode._parent._subNodes.length)
				checkNext(currentTextNode._parent._subNodes);

		} while(true);

		currentTextNode.text = content.substring(currentTextNode._index, content.length);

		function recrs(node){
			if(!node) return;
			for(var k in node) if(k.indexOf("_") == 0) delete node[k];
			for(var i = 0; node.children && node.children.length && i < node.children.length; i++) recrs(node.children[i]);
		}
		recrs(rootNode, 0);
		return rootNode;
	}
}
Minibar.Node = function Node(_index){this._index = _index;this.type = this.type+"";};
Minibar.Node.prototype.type = "Node";
Minibar.Block = function Block(_index){this.baseConstructor(_index);};
Minibar.Block.prototype.baseConstructor = Minibar.Node;
Minibar.Block.prototype.type = "Block";
Minibar.Block.prototype.push = function(node){
	node._parent = this;
	if(!this.children) this.children = [];
	this.children.push(node);
	return node;
};
Minibar.Root = Minibar.block({
	type : "Root"
});
Minibar.Label = Minibar.block({
	type : "Label",
	_open : /^(?!(?:(scriptoutput|else|[\/#@])))([^\s\n]{1,250})$/, 
	_selfClosing : true,
	_create : function(match){
		this.baseConstructor();
		if(match && match[0]) this.property = match[0].trim();
		else this.property = false;
	}
});
Minibar.ClientSideObject = Minibar.block({
	type : "ClientSideObject",
	_open : /^#addobject\s+([^\s]*)\s+([^\s]*)$/, 
	_selfClosing : true,
	_create : function(match){
		this.baseConstructor();
		if(match && match[1] && match[2]){
			this.key = match[1].trim();
			this.value = match[2].trim();
		}
	}
});
Minibar.PageContext = Minibar.block({
	type : "PageContext",
	_open : /^#pagecontext\s+([^\s]*)$/, 
	_selfClosing : true,
	_create : function(match){
		this.baseConstructor();
		if(match && match[1]){
			this.key = match[1].trim();
		}
	}
});
Minibar.ScriptOutput = Minibar.block({
	type : "ScriptOutput",
	_open : /^scriptoutput:1$/, 
	_selfClosing : true
});
Minibar.Rewrites = Minibar.block({
	type : "Rewrites",
	_open : /^#rewrites$/, 
	_close :/^\/rewrites$/,
	_create : function(match){
		this.baseConstructor();
	},
	_subNodes:[
		Minibar.Rewrite = Minibar.block({
			type : "Rewrite",
			_open : /^#rewrite\s+(.*?)\s+(.*)$/, 
			_selfClosing : true,
			_subnode : true,
			_create : function(match){
				this.baseConstructor();
				if(match[1] && match[2] && match[1].trim() && match[2].trim()){
					this.rProp = match[1].trim();
					this.rVal = match[2].trim();
				}
			}
		}),
	]
});
Minibar.Each = Minibar.block({
	type : "Each",
	_open : /^#each\s*(.*)$/, 
	_close :/^\/each$/,
	_create : function(match){
		this.baseConstructor();
		if(match && match[1]) this.property = match[1].trim();
		else this.property = false;
	},
	_subNodes:[
		Minibar.EachElse = Minibar.block({
			type : "EachElse",
			_open : /^else$/, 
			_close :/^\/each$/,
			_subnode : true,
			_onClose : function(endIndx){
				return this._parent._parent.push(new Minibar.Text(endIndx));
			}
		})		
	]
});
Minibar.If = Minibar.block({
	type : "If",
	_open : /^#if\s*(.*)$/, 
	_close :/^\/if$/,
	_create : function(match){
		this.baseConstructor();
		if(match && match[1]) this.property = match[1].trim();
		else this.property = false;
	},
	_subNodes:[
		Minibar.IfElse = Minibar.block({
			type : "IfElse",
			_open : /^else$/, 
			_close :/^\/if$/,
			_subnode : true,
			_onClose : function(endIndx){
				return this._parent._parent.push(new Minibar.Text(endIndx));
			}
		})
	]
});
Minibar.Unless = Minibar.block({
	type : "Unless",
	_open : /^#unless\s*(.*)$/, 
	_close :/^\/unless$/,
	_create : function(match){
		this.baseConstructor();
		if(match && match[1]) this.property = match[1].trim();
		else this.property = false;
	},
	_subNodes:[
		Minibar.IfElse = Minibar.block({
			type : "IfElse",
			_open : /^else$/, 
			_close :/^\/unless$/,
			_subnode : true,
			_onClose : function(endIndx){
				return this._parent._parent.push(new Minibar.Text(endIndx));
			}
		})
	]
});
Minibar.Text = Minibar.node({
	type : "Text",
});