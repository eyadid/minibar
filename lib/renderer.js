
var Tags = {};
Tags.Text = function(context, node, data, scope, writer){
	if(node.text != undefined)
		writer.write(node.text+"");
};
Tags.Label = function(context, node, data, scope, writer){
	if(!node || !node.property) return;
	var findRes = Renderer.find(context, data, node.property, scope);
	if(findRes && findRes[1] != undefined) {
		if(typeof(findRes[1]) == 'object')
			writer.write(Renderer.stringify(findRes[1])+"");
		else
			writer.write(findRes[1]+"");
	}
};
Tags.ClientSideObject = function(context, node, data, scope, writer){
	if(!context || !node || !node.key || !node.value) return;
	var findRes = Renderer.find(context, data, node.value, scope);
	if(!findRes || findRes[1] == undefined) return;
	if(!context.clientSideObjects) context.clientSideObjects = {};
	context.clientSideObjects[node.key] = findRes[1];
};
Tags.PageContext = function(context, node, data, scope, writer){
	if(!node || !node.key) return;
	if(data && data.pageContext && data.pageContext[node.key] != undefined)
		writer.write("$ctx.put(\"" + node.key + "\"," + Renderer.stringify(data.pageContext[node.key]) + ");");
};
Tags.ScriptOutput = function(context, node, data, scope, writer){
	writer.write("{");
	if(context && context.clientSideObjects != undefined){
		writer.write("if(!window[\"$objects\"]) window[\"$objects\"]={};");
		for(var o in context.clientSideObjects)
			writer.write("$objects[\"" + o + "\"]=" + Renderer.stringify(context.clientSideObjects[o]) + ";");
	}
	writer.write("}");
};
Tags.Each = function(context, node, data, scope, writer){
	if(!node || !node.property) return;
	var findRes = Renderer.find(context, data, node.property, scope);
	if(!findRes || findRes[1] == undefined || !findRes[0]){
		for(var ii = 0 ; node.children && ii < node.children.length; ii++){
			if(node.children[ii].type == "EachElse"){
				Renderer.render(context, node.children[ii], data, scope, writer);
				return;
			}
		}
		return;
	}

	var _old_rewrites = Tags._createContextRewrites(context);
	context._rewrites["iterator"] = "this";
	context._rewrites["index"] = "this.parent._iteratorKey";
	var scope = findRes[0].join(".");
	if(findRes[1].length){		
		for(var i = 0 ; i < findRes[1].length; i++){
			findRes[1]._iteratorKey = i;
			for(var ii = 0 ; node.children && ii < node.children.length; ii++){
				if(node.children[ii].type != "EachElse"){
					Renderer.render(context, node.children[ii], data, scope+"[" + i + "]", writer);
				}
			}
		}
	}else{
		for(var k in findRes[1]){
			findRes[1]._iteratorKey = k;
			for(var ii = 0 ; node.children && ii < node.children.length; ii++){
				if(node.children[ii].type != "EachElse"){
					Renderer.render(context, node.children[ii], data, scope+"." + k, writer);
				}
			}
		}
	}
	context._rewrites = _old_rewrites;
};
Tags.If = function(context, node, data, scope, writer){
	if(!node || !node.property) return;
	var findRes = Renderer.find(context, data, node.property, scope);
	for(var i = 0 ; node.children && i < node.children.length; i++){
		if((!findRes || !findRes[1]) && node.children[i].type == "IfElse"){
			Renderer.render(context, node.children[i], data, scope, writer);
			return;
		}else if(findRes && findRes[1] && node.children[i].type != "IfElse"){
			Renderer.render(context, node.children[i], data, scope, writer);
		}
	}
};
Tags.Unless = function(context, node, data, scope, writer){
	if(!node || !node.property) return;
	var findRes = Renderer.find(context, data, node.property, scope);
	for(var i = 0 ; node.children && i < node.children.length; i++){
		if((!findRes || !findRes[1]) && node.children[i].type != "IfElse"){
			Renderer.render(context, node.children[i], data, scope, writer);
		}else if(findRes && findRes[1] && node.children[i].type == "IfElse"){
			Renderer.render(context, node.children[i], data, scope, writer);
		}
	}
};
Tags._createContextRewrites = function(context){
	var o = context._rewrites;
	context._rewrites = {};
	for(var k in o) context._rewrites[k] = o[k];
	return o;
};
Tags.Rewrites = function(context, node, data, scope, writer){
	if(!node) return;
	var _old_rewrites = Tags._createContextRewrites(context);
	for(var i = 0 ; node.children && i < node.children.length; i++)
		if(node.children[i].type == "Rewrite" && node.children[i].rVal && node.children[i].rProp)
			context._rewrites[node.children[i].rProp] = node.children[i].rVal;
	for(var i = 0 ; node.children && i < node.children.length; i++)
		Renderer.render(context, node.children[i], data, scope, writer);
	context._rewrites = _old_rewrites;
};
Tags.EachElse =
Tags.IfElse = 
Tags.Root = function(context, node, data, scope, writer){
	if(!node) return;
	for(var i = 0 ; node.children && i < node.children.length; i++)
		Renderer.render(context, node.children[i], data, scope, writer);
};

var Renderer = {
	splitPathFindArr : /^(.*)\[([0-9]*)\]$/,
	splitPath : function(str){
		if(!str) return [];
		var res = [];
		var s = str.split(".");
		for(var i = 0; i < s.length; i++){
			if(!s[i] || !s[i].trim()) continue;
			var m = s[i].match(Renderer.splitPathFindArr);
			if(m && m[1] && m[2] && !isNaN(parseInt(m[2]))){
				res.push(m[1]);
				res.push(m[2]);
			}else{
				res.push(s[i]);
			}
		}
		return res;
	},
	find : function(context, data, str, scope){
		var requestedPath = Renderer.splitPath(str);
		if(!requestedPath.length) return null;
		var checked = [];
		while(true){
			if(!context._rewrites[requestedPath[0]] || checked.indexOf(requestedPath[0]) != -1)
				break;
			var rewrites = Renderer.splitPath(context._rewrites[requestedPath[0]]);
			if(!rewrites.length || rewrites[0] == requestedPath[0])
				break;
			checked.push(requestedPath[0]);
			requestedPath.shift();
			requestedPath = rewrites.concat(requestedPath);
		}
		var path = Renderer.splitPath(scope).concat(requestedPath);
		for(var i = 0 ; i < path.length; i++){			
			if(path[i] == "this"){
				path.splice(i,1);
				i = -1;
				continue;
			} else if(path[i] == "parent"){
				if(i == 0) return null;
				path.splice(i-1,2);
				i = -1;
				continue;
			} else if(path[i] == "root"){
				path.splice(0,(i > 0 ? i : 1));
				i = -1;
				continue;
			}
		}

		if(context && context._defaultBase && path[0] != context._defaultBase)
			path.unshift(context._defaultBase);
		var obj = data;
		for(var i = 0 ; i < path.length; i++){
			if(!obj) return null;
			obj = obj[path[i]];
		}
		if(obj == undefined) return null;

		return [path, obj];
	},
	render : function(context, node, data, scope, writer){
		if(!node) return;
		if(node.type == "Root" && !context) context = {_defaultBase:"pageData"};
		if(!context._rewrites) context._rewrites = {"pageData":"root"};
		if(Tags[node.type]) Tags[node.type](context, node, data, scope, writer);		
	},
	stringify : function(o){
		var cache = [];
		var r = JSON.stringify(o, function(key, value) {
			if (typeof value === 'object' && value !== null){
				if (cache.indexOf(value) !== -1) return;
				cache.push(value);
			}
			return value;
		});
		cache = null; 
		delete cache;
		return r;
	}
};

module.exports = Renderer;