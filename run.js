const   Minibar = require("./lib/minibar"),
        Renderer = require("./lib/renderer"),
        fs = require("fs");

/* ******************************************************************************************************************************************************************************************************************

        TODO - Your worker should parse the URL and decide which 'payload' and which 'template' are needed.
        Then make 2 HTTP requests, one to fetch the html template and one to get the payload (dynamic data)

        In our case, we cache the html template in the CloudFlare Caching layer (internal caching system they expose to workers)
        and the payload content is either on S3 or on a webserver with EFS (in your case, it`ll be a webserver with redis)

****************************************************************************************************************************************************************************************************************** */
    
const payload = JSON.parse(fs.readFileSync("./payload.json","utf-8"));
const template = Minibar.parse(fs.readFileSync("./template.html","utf-8")); 

/// Once you have the template and the payload, call this method to render the HTML.
// In this case, we just write it to disk as "response.html"

let wrote = false;
Renderer.render(null, template, payload, "", {
    write : function(buff){// This will be called for 'writes' - in our worker, we just write it directly to the response
        if(wrote){            
            fs.appendFileSync("./response.html", buff);
        }else{
            fs.writeFileSync("./response.html", buff);
            wrote = true;
        }
    }
});

console.log("Response Done!");